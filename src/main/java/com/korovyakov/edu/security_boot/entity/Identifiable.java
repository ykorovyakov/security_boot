package com.korovyakov.edu.security_boot.entity;

import lombok.AccessLevel;
import lombok.Getter;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Getter(AccessLevel.PROTECTED)
public abstract class Identifiable {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
}
