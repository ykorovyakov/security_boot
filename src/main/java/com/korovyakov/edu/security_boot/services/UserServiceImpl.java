package com.korovyakov.edu.security_boot.services;

import com.korovyakov.edu.security_boot.api.repository.UserRepository;
import com.korovyakov.edu.security_boot.api.service.UserService;
import com.korovyakov.edu.security_boot.entity.Role;
import com.korovyakov.edu.security_boot.entity.User;
import com.korovyakov.edu.security_boot.types.RoleType;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.Collections;

@Data
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public User saveUser(String login) {
        User user = findUser(login);
        if(user != null){
            return user;
        }
        user = new User();
        user.setLogin(login);
        userRepository.save(user);
        return user;
    }

    private User findUser(String login){
        return userRepository.findByLogin(login);
    }

    @PostConstruct
    private void init(){
        initUser("admin", "admin", RoleType.ROLE_ADMIN);
        initUser("user", "user", RoleType.ROLE_USER);
    }

    @Transactional
    public void initUser(String login, String password, RoleType roleType){
        User user = userRepository.findByLogin(login);
        if(user != null) return;
        user = new User();
        user.setLogin(login);
        user.setPass(passwordEncoder.encode(password));
        Role role = new Role();
        role.setName(roleType.name());
        user.setRoles(Collections.singleton(role));
        userRepository.save(user);
    }
}
