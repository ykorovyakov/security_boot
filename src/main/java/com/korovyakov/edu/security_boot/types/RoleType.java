package com.korovyakov.edu.security_boot.types;

public enum RoleType {
    ROLE_ADMIN,
    ROLE_USER;
}
