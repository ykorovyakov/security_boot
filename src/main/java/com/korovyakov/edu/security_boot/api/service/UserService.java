package com.korovyakov.edu.security_boot.api.service;

import com.korovyakov.edu.security_boot.entity.User;

public interface UserService {
    User saveUser(String login);
}
